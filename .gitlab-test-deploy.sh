#!/bin/bash
set -f
echo "Deploy project on server $TEST_SERVER"
ssh ec2-usere@$TEST_SERVER "/opt/project/deploy/test.sh"