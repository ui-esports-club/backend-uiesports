python3.8 ~/uiesports/backend-uiesports/manage.py collectstatic --noinput --settings=backend_uiesports.settings.prod 
python3.8 ~/uiesports/backend-uiesports/manage.py makemigrations --settings=backend_uiesports.settings.prod 
python3.8 ~/uiesports/backend-uiesports/manage.py migrate --settings=backend_uiesports.settings.prod 

#python3.8 ~/uiesports/backend-uiesports/manage.py createsuperuser --noinput --username uiesports-admin --email noreplyuiesports@gmail.com --password kakiku123

#cd ~/uiesports/backend-uiesports
gunicorn backend_uiesports.wsgi --env DJANGO_SETTINGS_MODULE=backend_uiesports.settings.prod -b 0.0.0.0:8000 --timeout 900 --log-level debug --log-file -
