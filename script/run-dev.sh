python /app/manage.py collectstatic --noinput
python /app/manage.py makemigrations
python /app/manage.py migrate

python manage.py runserver 0.0.0.0:8000 --settings=backend_uiesports.settings.staging-docker