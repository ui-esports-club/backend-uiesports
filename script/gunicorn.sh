python /app/manage.py --settings=backend_uiesports.settings.staging-docker collectstatic --noinput
python /app/manage.py --settings=backend_uiesports.settings.staging-docker makemigrations
python /app/manage.py --settings=backend_uiesports.settings.staging-docker migrate

python /app/manage.py manage.py createsuperuser --noinput --username uiesports-admin --email noreplyuiesports@gmail.com --password kakiku123

gunicorn backend_uiesports.wsgi --env DJANGO_SETTINGS_MODULE=backend_uiesports.settings.staging-docker -b 0.0.0.0:8000 --timeout 900 --chdir=/app --log-level debug --log-file -