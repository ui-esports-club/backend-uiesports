from django.db import models

# Create your models here.
class TestUser (models.Model):
    username = models.TextField(max_length='255')
    password = models.TextField(max_length='255')
class ImageTest(models.Model):
    image = models.ImageField()
    name = models.CharField(max_length=100)
