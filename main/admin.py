from django.contrib import admin
from .models import TestUser
from . import models

admin.site.register(TestUser)
# Register your models here.
admin.site.register(models.ImageTest)