import random
from drf_yasg import openapi
from rest_framework.views import APIView
from authentication.models import AuthUser
from .serializers import ProfileSerializer
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication
from backend_uiesports.constants import API_STANDARD_RESPONSE_400, API_STANDARD_RESPONSE_200

# Create your views here.

class ProfileAPI(APIView):
    permission_classes = [IsAuthenticated]
    authentication_classes = [JWTAuthentication]

    @swagger_auto_schema(
        responses={200: ProfileSerializer(many=True), 400: API_STANDARD_RESPONSE_400}
    )
    def get(self, request):
        data = request.user
        serializer = ProfileSerializer(data)
        return Response(serializer.data)
    
    @swagger_auto_schema(
        responses={200: API_STANDARD_RESPONSE_200, 400: API_STANDARD_RESPONSE_400},
        operation_description= "Edit User Profile"
    )
    def patch(self, request):
        data = request.data

        try:
            account = AuthUser.objects.get(username=request.user)
        except:
            return Response({"status": "Failed", "status_code": 400})
        
        serializer = ProfileSerializer(account, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({"status": "OK", "status_code": 200})
        
        return Response({"status": "Failed", "status_code": 400})




PROFILE_COMPLETION_RES_200 = openapi.Response(
    description="OK",
    examples={
        "application/json": {k.name: random.choice([True, False]) for k in AuthUser._meta.fields}
    }
)

class ProfileCompletion(APIView):
    permission_classes = [IsAuthenticated]
    authentication_classes = [JWTAuthentication]

    @swagger_auto_schema(responses={200: PROFILE_COMPLETION_RES_200})
    def get(self, request):
        data = request.user
        serializer = ProfileSerializer(data)

        completion_data = {}
        for k,v in serializer.data.items():
            if v == None or v == '':
                completion_data[k] = False
            else:
                completion_data[k] = True

        return Response(completion_data)
