from . import views
from django.urls import include, path

urlpatterns = [
    path('', views.ProfileAPI.as_view(), name='profile'),
    path('completion', views.ProfileCompletion.as_view(), name='profile-completion')
]