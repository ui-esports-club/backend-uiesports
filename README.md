# backend_uiesports
### __How To Run on Local Development (Without Docker)__ 
1. Buat virtual environment `python -m venv venv`
2. Masuk ke venv `venv\Scripts\activate`
3. Install dependencies `pip install -r requirements.txt`
4. Running aplikasi dengan command `python manage.py runserver --settings=backend_uiesports.settings-dev`

### __How To Run on Local Development (With Docker)__ 
1. Pastikan sudah menginstall docker pada PC/Laptop anda
2. Pada Development, gunakan file docker-compose-dev.yml untuk menjalankan aplikasi
    ```bash
    docker-compose -f docker-compose-dev.yml up -d --build
    ```
3. Server akan running pada localhost:8000