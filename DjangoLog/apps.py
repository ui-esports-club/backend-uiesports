from django.apps import AppConfig


class DjangologConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'DjangoLog'
