from rest_framework.views import exception_handler
import logging
import json
import traceback


def custom_exception_handler_function(exc, context):
    request = context['request']
    logger = logging.getLogger('DjangoLog')
    message = {
        'method' : f'{request.method}',
        'url' : f'{request.build_absolute_uri()}',
        'traceback': f'{traceback.format_exc()}'
    }

    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    # Now add the HTTP status code to the response.
    if response is not None:
        message['status_code'] = response.status_code
        message['message'] = str(exc)
        logger.error(json.dumps(message))

    return response