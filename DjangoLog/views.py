from asyncio.log import logger
import json
import logging
import requests

from urllib.request import Request
from django.shortcuts import render

logger = logging.getLogger('DjangoLog')

# Create your views here.
class DiscordInterface:
    def _send(self,payload):
        requests.post(
            url= "https://discord.com/api/webhooks/998974289938231387/SRVRzdvtgmQVRVDwflbqxkX18-cu0vsFwGUBIcM8KgZo01VVDUIRBqODCgLHJbstCv0L",
            data=payload,
            headers={'content-type': 'application/json'}
        )

    def message_error(self, message):
        message = json.loads(message)
        data = {
            "embeds": [{
                "fields": [
                    {
                        "name": "Message",
                        "value": message['message']
                    },
                    {
                        "name": "Status Code",
                        "value": message['status_code']
                    },
                    {
                        "name": "Url",
                        "value": message['url']
                    },
                    {
                        "name": "Method",
                        "value": message['method']
                    },
                    {
                        "name": "Traceback",
                        "value": f'```{message["traceback"]}```'
                    }
                ]
            }]
        }
        self._send(payload=json.dumps(data))

class DiscordHandler(logging.Handler):
    def emit(self, record):
        self.format(record)
        discord_webhook = DiscordInterface();
        if record.levelno == logging.ERROR:
            discord_webhook.message_error(record.message)
        else:
            discord_webhook.message_success(record.message)