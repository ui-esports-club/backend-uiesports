from django.db import models
from authentication.models import AuthUser, Divisi


# Create your models here.
class Team(models.Model):
    nama_team = models.CharField(max_length=50, default="UI Esports Team")
    logo_team = models.ImageField(null=True, blank=True)
    foto_team = models.ImageField(null=True, blank=True)
    divisi = models.ForeignKey(Divisi, on_delete=models.CASCADE, null=True)


    def __str__(self):
        return self.nama_team



class PlayerInfo(models.Model):
    nickname_player = models.CharField(max_length=50, default="UI Esports Player")
    id_player = models.CharField(max_length=50, null=True)
    user = models.ForeignKey(AuthUser, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE, null=True, blank=True)


    def __str__(self):
        return self.nickname_player


class Achievement(models.Model):
    nama_achievement = models.CharField(max_length=120, default="Juara 1")
    url_sertifikat = models.ImageField(null=True, blank=True)
    team = models.ForeignKey(Team, on_delete=models.CASCADE, null=True)


    def __str__(self):
        return self.nama_achievement


class Category(models.Model):
    category_id = models.CharField(max_length=50, primary_key=True)
    name = models.CharField(max_length=50, default="Nama Kategori")
    contact_person = models.CharField(max_length=50, default="Contact Person")


class Product(models.Model):
    product_id = models.CharField(max_length=50, primary_key=True)
    name = models.CharField(max_length=100, default="Nama Produk")
    price = models.IntegerField(default=0)
    category_fk = models.ForeignKey(Category, on_delete=models.CASCADE)


class Status(models.Model):
    status_id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=30)


class Order(models.Model):
    order_id = models.IntegerField(primary_key=True)
    customer_name = models.CharField(max_length=100, default="Customer Name")
    customer_contact = models.CharField(max_length=100, default="Contact")
    product_fk = models.ForeignKey(Product, on_delete=models.CASCADE)
    customer_email = models.EmailField(default="example@mail.com")
    customer_address = models.CharField(max_length=255, default="Customer Address")
    description = models.CharField(max_length=50, null=True, blank=True)
    status = models.ForeignKey(Status, on_delete=models.CASCADE)
    order_code = models.CharField(max_length=50, default="default order code")
    order_date = models.DateField(auto_now_add=True)