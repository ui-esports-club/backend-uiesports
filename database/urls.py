from os import stat
from django.urls import path
from .views import MemberViewAll, MemberDetailView, ProductViewAll, StatusViewAll, OrderViewAll, CategoryViewAll, ProductViewDetail, StatusViewDetail, OrderViewDetail, CategoryViewDetail

app_name = "database"

urlpatterns = [
    path("all", MemberViewAll.as_view(), name="all_member"),
    path("member/<int:id>", MemberDetailView.as_view(), name="detail_member"),
    path("product", ProductViewAll.as_view(), name="all_products"),
    path("status", StatusViewAll.as_view(), name="all_status"),
    path("order", OrderViewAll.as_view(), name="all_orders"),
    path("category", CategoryViewAll.as_view(), name="all_categories"),
    path("product/<int:id>", ProductViewDetail.as_view(), name="detail_product"),
    path("status/<int:id>", StatusViewDetail.as_view(), name="detail_status"),
    path("order/<int:id>", OrderViewDetail.as_view(), name="detail_order"),
    path("category/<int:id>", CategoryViewDetail.as_view(), name="detail_category"),
]