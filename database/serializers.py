from ast import Mod
from .models import Team, PlayerInfo, Achievement, Category, Product, Status, Order
from authentication.models import AuthUser, Divisi
from rest_framework.generics import GenericAPIView, ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.serializers import ModelSerializer


class DivisiSerializer(ModelSerializer):
    class Meta:
        model = Divisi
        fields = ['nama_divisi']

class MemberSerializer(ModelSerializer):
    divisi = DivisiSerializer(many=True)

    class Meta:
        model = AuthUser
        fields = ('id', 'username', 'first_name', 'last_name', 'fakultas', 'npm', 'id_line', 'email', 'instagram', 'divisi', 'angkatan', 'photo', 'tag')


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ('category_id', 'name', 'contact_person')


class ProductSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = ('product_id', 'name', 'price', 'category_fk')


class StatusSerializer(ModelSerializer):
    class Meta:
        model = Status
        fields = ('status_id', 'name')


class OrderSerializer(ModelSerializer):
    class Meta:
        model = Order
        fields = ('order_id', 'customer_name', 'customer_contact', 'product_fk', 'customer_email', 'customer_address', 'description', 'status', 'order_code', 'order_date')