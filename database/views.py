from rest_framework.generics import RetrieveAPIView, CreateAPIView, UpdateAPIView, ListAPIView, RetrieveUpdateDestroyAPIView
from authentication.models import AuthUser
from .models import Category, Product, Status, Order
from .serializers import MemberSerializer, CategorySerializer, ProductSerializer, StatusSerializer, OrderSerializer
from rest_framework.pagination import PageNumberPagination
from rest_framework.filters import SearchFilter
from rest_framework.permissions import AllowAny
from django_filters.rest_framework import DjangoFilterBackend


class MemberPagination(PageNumberPagination):
    page_size = 8

class ProductPagination(PageNumberPagination):
    page_size = 20

class MemberViewAll(ListAPIView):
    queryset = AuthUser.objects.all()
    serializer_class = MemberSerializer
    pagination_class = MemberPagination
    filter_backends = [SearchFilter, DjangoFilterBackend]
    filterset_fields = ['divisi__nama_divisi']
    search_fields = ['username', 'tag']

class MemberDetailView(RetrieveAPIView):
    queryset = AuthUser.objects.all()
    serializer_class = MemberSerializer
    lookup_field = "id"


class CategoryViewAll(ListAPIView):
    # Auth Setting
    permission_classes = [AllowAny]

    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    filter_backends = [SearchFilter, DjangoFilterBackend]

class CategoryViewDetail(RetrieveAPIView):
    # Auth Setting
    permission_classes = [AllowAny]

    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    lookup_field = "category_id"


class ProductViewAll(ListAPIView):
    # Auth Setting
    permission_classes = [AllowAny]

    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    pagination_class = ProductPagination
    filter_backends = [SearchFilter, DjangoFilterBackend]
    filterset_fields = ['name']
    search_fields = ['name', 'category']


class ProductViewDetail(RetrieveAPIView):
    # Auth Setting
    permission_classes = [AllowAny]


    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    lookup_field = "product_id"


class StatusViewAll(ListAPIView):
    # Auth Setting
    permission_classes = [AllowAny]

    queryset = Status.objects.all()
    serializer_class = StatusSerializer

class StatusViewDetail(RetrieveAPIView):
    # Auth Setting
    permission_classes = [AllowAny]

    queryset = Status.objects.all()
    serializer_class = StatusSerializer
    lookup_field = "status_id"


class OrderViewAll(ListAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filter_backends = [SearchFilter, DjangoFilterBackend]

class OrderViewDetail(RetrieveAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    lookup_field = "order_id" 

