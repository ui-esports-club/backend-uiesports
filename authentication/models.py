from email.policy import default
import os
from .fields import WEBPField
from django.db import models
from django.contrib.auth.models import AbstractUser



# Create your models here.
class Divisi(models.Model):
    nama_divisi = models.CharField(max_length=50)
    tipe = models.CharField(max_length=15)

    def __str__(self):
        return self.nama_divisi


def content_file_name(instance, filename):
    filename = f'{instance.username}.webp'
    return os.path.join('profile', filename)

class AuthUser(AbstractUser):
    is_admin = models.BooleanField(default=False, verbose_name="admin", blank=True, null=True)
    no_telp = models.CharField(max_length=20, blank=True, null=True)
    id_line = models.CharField(max_length=50, blank=True, null=True)
    whatsapp = models.CharField(max_length=20, blank=True, null=True)
    instagram = models.CharField(max_length=20, blank=True, null=True)
    npm = models.CharField(max_length=10, blank=True, null=True)
    fakultas = models.CharField(max_length=50, blank=True, null=True)
    jurusan = models.CharField(max_length=50, blank=True, null=True)
    # photo = models.ImageField(upload_to=content_file_name, blank=True, null=True)
    photo = WEBPField(
        upload_to=content_file_name,
        blank=True,
        null=True,
        default="profile/default-photo.webp"
    )
    angkatan = models.IntegerField(blank=True, null=True)
    tag = models.CharField(max_length=255, blank=True, null=True)
    divisi = models.ManyToManyField(Divisi, blank=True, null=True)