from . import views
from django.urls import include, path

urlpatterns = [
    path('auth/jwt/token', views.TokenObtainPairView.as_view(), name='token'),
    path('auth/jwt/refresh', views.TokenRefreshView.as_view(), name='refresh'),
    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.jwt'))
]