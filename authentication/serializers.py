from rest_framework_simplejwt.serializers import TokenObtainPairSerializer, TokenRefreshSerializer
from rest_framework_simplejwt.tokens import RefreshToken
# from .models import Administrator, Staff
from rest_framework import serializers

class TokenObtainLifetimeSerializer(TokenObtainPairSerializer):

    def validate(self, attrs):
        data = super().validate(attrs)
        refresh = self.get_token(self.user)
        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)

        if self.user.is_admin:
            data['role'] = 'Administrator'
            return data

        else:
            data['role'] = 'Staff'
            return data

class TokenRefreshLifetimeSerializer(TokenRefreshSerializer):

    def validate(self, attrs):
        data = super().validate(attrs)
        refresh = RefreshToken(attrs['refresh'])
        
        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)
        
        return data