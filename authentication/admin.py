from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from . import models

# Register your models here.
class CustomUserAdmin(UserAdmin):
    list_display = (
        'username', 'email', 'first_name', 'last_name',
        'is_staff', 'is_admin', 'no_telp', 'id_line', 'whatsapp', 'instagram',
        'npm', 'fakultas', 'jurusan', 'photo', 'angkatan', 'tag',
    )

    fieldsets = (
        (None, {
            'fields': ('username', 'password')
        }),
        ('Personal info', {
            'fields': (
                'first_name', 'last_name', 'email',
                'no_telp', 'id_line', 'whatsapp', 'instagram',
                'npm', 'fakultas', 'jurusan', 'photo',
                'angkatan', 'tag', 'divisi'
            )
        }),
        ('Permissions', {
            'fields': (
                'is_active', 'is_staff', 'is_superuser', 'is_admin',
                'groups', 'user_permissions'
                )
        })
    )

    add_fieldsets = (
        (None, {
            'fields': ('username', 'password1', 'password2')
        }),
        ('Personal info', {
            'fields': (
                'first_name', 'last_name', 'email',
                'no_telp', 'id_line', 'whatsapp', 'instagram',
                'npm', 'fakultas', 'jurusan', 'photo',
                'angkatan', 'tag', 'divisi'
            )
        }),
        ('Permissions', {
            'fields': (
                'is_active', 'is_staff', 'is_superuser',
                'is_admin','groups', 'user_permissions'
                )
        })
    )


admin.site.register(models.AuthUser, CustomUserAdmin)
admin.site.register(models.Divisi)