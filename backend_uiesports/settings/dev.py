from .base import *

DEBUG = True
ALLOWED_HOSTS = ['*']

# Djoser domain and sitename
DOMAIN = 'localhost:3000' #example.com
SITE_NAME = 'frontend-uiesports' #Example

