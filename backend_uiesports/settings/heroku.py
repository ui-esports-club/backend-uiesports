from .base import *
import dj_database_url

DEBUG = True
ALLOWED_HOSTS = ['*']

MIDDLEWARE += [
    'corsheaders.middleware.CorsMiddleware',
]

# Heroku (staging) config
if os.getenv('DATABASE_URL', None) is not None:
    DATABASES['default'] = dj_database_url.config(
        conn_max_age=600, ssl_require=True
    )

# Djoser domain and sitename
DOMAIN = 'frontend-uiesports-staging.herokuapp.com' #example.com
SITE_NAME = 'frontend-uiesports' #Example

# Cors Header Settings
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True


# GCS Config
from google.oauth2 import service_account
GS_CREDENTIALS = service_account.Credentials.from_service_account_file('credential.json')
DEFAULT_FILE_STORAGE = 'storages.backends.gcloud.GoogleCloudStorage'
GS_BUCKET_NAME = 'uiesports'
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
