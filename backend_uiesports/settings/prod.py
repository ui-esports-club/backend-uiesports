from .base import *
import os

DATABASES = {
    "default": {
        "ENGINE": 'django.db.backends.postgresql',
        "NAME": "postgres",
        "USER": "postgres",
        "HOST": "localhost",
        "PORT": '5432',
        "PASSWORD":"kakiku123",
    }
}


DEBUG = True
ALLOWED_HOSTS = ['*']

# Djoser domain and sitename
DOMAIN = 'localhost:3000' #example.com
SITE_NAME = 'frontend-uiesports' #Example
