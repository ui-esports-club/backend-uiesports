from .base import *
import os

DATABASES = {
    "default": {
        "ENGINE": 'django.db.backends.postgresql',
        "NAME": os.environ.get("PGDATABASE"),
        "USER": os.environ.get("PGUSER", "uiesports"),
        "PASSWORD": os.environ.get("PGPASSWORD", "uiesports2022"),
        "HOST": os.environ.get("PGHOST"),
        "PORT": '5432',
    }
}


DEBUG = True
ALLOWED_HOSTS = ['*']

# Djoser domain and sitename
DOMAIN = 'localhost:3000' #example.com
SITE_NAME = 'frontend-uiesports' #Example

