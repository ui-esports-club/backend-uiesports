from drf_yasg import openapi

API_STANDARD_RESPONSE_200 = openapi.Response(description="OK", examples={
                                "application/json": {"status": "OK","status_code": 200}
                            })

API_STANDARD_RESPONSE_400 = openapi.Response(description="Failed", examples={
                                "application/json": {"status": "Failed","status_code": 400}
                            })